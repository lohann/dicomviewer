//
//  ViewController.m
//  ROI Finder
//
//  Created by Lohann Paterno Coutinho Ferreira on 15/09/14.
//  Copyright (c) 2014 Lohann Paterno Coutinho Ferreira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
            
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
