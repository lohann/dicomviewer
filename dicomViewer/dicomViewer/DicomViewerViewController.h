//
//  DicomViewerViewController.h
//  dicomViewer
//
//  Created by Alin Brindusescu on 6/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DicomViewerViewController : UIViewController
@property (retain, nonatomic) IBOutlet UILabel *patientName;
@property (retain, nonatomic) IBOutlet UILabel *imageSizeInfo;
@property (retain, nonatomic) IBOutlet UIImageView *patientImageView;
- (IBAction)onLoadDicom:(id)sender;

@end
