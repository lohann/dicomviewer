//
//  DicomViewerAppDelegate.h
//  dicomViewer
//
//  Created by Alin Brindusescu on 6/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DicomViewerViewController;

@interface DicomViewerAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) DicomViewerViewController *viewController;

@end
