//
//  DicomViewerViewController.m
//  dicomViewer
//
//  Created by Alin Brindusescu on 6/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DicomViewerViewController.h"

#include <dcmtk/config/osconfig.h>
#include <dcmtk/dcmdata/dcfilefo.h>
#include <dcmtk/dcmdata/dcdeftag.h>
#include <dcmtk/dcmimgle/dcmimage.h>


@implementation DicomViewerViewController
@synthesize patientName;
@synthesize imageSizeInfo;
@synthesize patientImageView;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setPatientName:nil];
    [self setPatientImageView:nil];
    [self setImageSizeInfo:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)onLoadDicom:(id)sender
{
//	NSString *dcmFilename           = [[NSBundle mainBundle] pathForResource:@"38" ofType:@"dcm"];
//    
//    DcmFileFormat fileFormat;
//    OFCondition loadStatus = fileFormat.loadFile([dcmFilename UTF8String]);
//    
//    if (loadStatus.good())   
//    {
//        OFString patientName;
//        OFCondition findStatus = fileFormat.getDataset()->findAndGetOFString(DCM_PatientName, patientName);
//        
//        if (findStatus.good())   
//        {
//            NSString *name = [NSString stringWithUTF8String:patientName.c_str()];
//            NSLog(@" >> %@ patient name is [%@]", dcmFilename, name);
//            self.patientName.text = [NSString stringWithFormat:@"Patient Name: %@", name];
//        }
//        
//        long int width, height, pixelDepth;
//        fileFormat.getDataset()->findAndGetLongInt(DCM_Columns, width);
//        fileFormat.getDataset()->findAndGetLongInt(DCM_Rows, height);
//        fileFormat.getDataset()->findAndGetLongInt(DCM_SamplesPerPixel, pixelDepth);
//        NSString *imageSizeInfo = [NSString stringWithFormat:@"Image Size: %ld x %ld %ld bytes", width, height, pixelDepth];
//        self.imageSizeInfo.text = imageSizeInfo;
//    }
//    else 
//    {
//        
//        NSLog(@" >> Failded to load %@!", dcmFilename);
//        
//    }
    

        OFLog::configure(OFLogger::INFO_LOG_LEVEL);
    
        NSString *dcmFilename = [[NSBundle mainBundle] pathForResource:@"38" ofType:@"dcm"];
        //DcmFileFormat fileFormat;
        //OFCondition loadStatus = fileFormat.loadFile([dcmFilename UTF8String]);
        DcmFileFormat *dfile = new DcmFileFormat();
        OFCondition cond = dfile->loadFile([dcmFilename UTF8String]);
        E_TransferSyntax xfer = dfile->getDataset()->getOriginalXfer();
        DicomImage *di = new DicomImage(dfile, xfer, CIF_AcrNemaCompatibility, 0, 1);
        //DicomImage *image = new DicomImage(dfile, CIF_UsePartialAccessToPixelData, 0, 10 /* fcount */);
        NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [path[0] stringByAppendingString:@"/teste.bmp"];
    
        di->writeBMP([filePath UTF8String], 24);
        patientImageView.image = [UIImage imageWithContentsOfFile:filePath];

}

- (void)dealloc {
    [patientName release];
    [patientImageView release];
    [imageSizeInfo release];
    [super dealloc];
}
@end
