//
//  main.m
//  ROIFinder
//
//  Created by Lohann Paterno Coutinho Ferreira on 15/09/14.
//  Copyright (c) 2014 Lohann Paterno Coutinho Ferreira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
