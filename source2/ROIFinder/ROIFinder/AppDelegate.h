//
//  AppDelegate.h
//  ROIFinder
//
//  Created by Lohann Paterno Coutinho Ferreira on 15/09/14.
//  Copyright (c) 2014 Lohann Paterno Coutinho Ferreira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

