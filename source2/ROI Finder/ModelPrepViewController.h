//
//  ModelPrepViewController.h
//  RoiFinder
//
//  Created by Leonardo Custodio on 10/1/14.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ModelPrepViewController : UIViewController

@property BOOL drag;
@property CGFloat movedXaxis;

@end
