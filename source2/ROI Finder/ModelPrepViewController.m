//
//  ModelPrepViewController.m
//  RoiFinder
//
//  Created by Leonardo Custodio on 10/1/14.
//
//

#import "ModelPrepViewController.h"

@interface ModelPrepViewController ()
{
    NSURL *url;
    AVPlayer *player;
}

@end

@implementation ModelPrepViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self playVideo:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)playVideo:(NSInteger)videoNumber
{
    url = [[NSBundle mainBundle] URLForResource:[NSString stringWithFormat:@"%d", videoNumber] withExtension:@"mp4"];
    //    _moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    //    [_moviePlayer.view setFrame:CGRectMake(0, 0, _videoView.frame.size.width, _videoView.frame.size.height)];
    //    [_moviePlayer prepareToPlay];
    //    _moviePlayer.controlStyle = MPMovieControlStyleNone;
    //    _moviePlayer.repeatMode = YES;
    //    [_moviePlayer setShouldAutoplay:YES]; // And other options you can look through the documentation.
    //    [_videoView addSubview:_moviePlayer.view];
    
    player = [[AVPlayer alloc] initWithURL:url];
    AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
    
    player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loopPlay:) name:AVPlayerItemDidPlayToEndTimeNotification object:[player currentItem]];
    
    // Gesture Recognizer
    
    
    
    playerLayer.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view.layer addSublayer:playerLayer];
    [playerLayer setNeedsDisplay];
    [player pause];
    [player seekToTime:CMTimeMakeWithSeconds(0.2, NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    
    self.navigationController.navigationBar.hidden = YES;
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playBackFinished) name:MPMoviePlayerPlaybackDidFinishNotification object:_moviePlayer];
}

- (void)loopPlay:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:self.view];
    NSLog(@"Began: (%f, %f)", location.x, location.y);
    
    if (CGRectContainsPoint(self.view.frame, location)) {
        _drag = YES;
        _movedXaxis = location.x;
        
        [player pause];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:self.view];
    //    static float tempo = 5;
    //    static const float tempoMaximo = 9;
    static float value = 1;
    static const float minValue = 0;
    static const float maxValue = 80;
    float duration = CMTimeGetSeconds(player.currentItem.duration);
    
    if (CMTIME_IS_INVALID(player.currentItem.duration)) {
        return;
    }
    
    static BOOL teste = true;
    if (_drag && teste) {
        teste = false;
        CGFloat moved = _movedXaxis - location.x;
        _movedXaxis = location.x;
        
        if (moved > 0)
        {
            NSLog(@"Moved left");
            
            if (isfinite(duration)) {
                value++;
                
                //double time = duration * (value - minValue) / (maxValue - minValue);
                
                if (value > maxValue)
                    value -= maxValue;
                if (value < minValue)
                    value += maxValue;
                double time = (value/maxValue) * duration;
                
                NSLog(@"L Value: %f - Time: %f", value, time);
                
                [player seekToTime:CMTimeMakeWithSeconds(time, NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
            }
            
        }
        else
        {
            NSLog(@"Moved right");
            
            if (isfinite(duration)) {
                value--;
                
                //double time = duration * (value - minValue) / (maxValue - minValue);
                
                if (value > maxValue)
                    value -= maxValue;
                if (value < minValue)
                    value += maxValue;
                double time = (value/maxValue) * duration;
                
                NSLog(@"Value: %f - Time: %f", value, time);
                
                [player seekToTime:CMTimeMakeWithSeconds(time, NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
            }
            
        }
        [NSThread sleepForTimeInterval:0.07];
        teste = true;
    }
    
}

@end
